#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import igor

class TestBlah(unittest.TestCase):
    def test_parseKeywords(self):
        f = igor.parseKeywords

        teststr = """foo=blah bar=23 baz=12.5 quux = "ding \\\\ \\\" dong" test = 1 """
        parsed = f(teststr)
        expected = {"foo": "blah", "bar": 23, "baz": 12.5,
                    "quux": "ding \\ \" dong",
                    "test": 1}
        self.assertEqual(parsed, expected)

if __name__ == '__main__':
    unittest.main()
